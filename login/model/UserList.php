<?php

namespace login\model;


require_once("UserCredentials.php");
require_once("common/model/PHPFileStorage.php");

/**
 * represents All users in the system
 *
 */
class UserList {
	/**
	 * Temporary solution with only one user "Admin" in PHPFileStorage
 	 * You might want to use a database instead.
	 * @var \common\model\PHPFileStorage
	 */
	private $adminFile;

	/**
	 * We only have one user in the system right now.
	 * @var array of UserCredentials
	 */
	private $users;


	public function  __construct( ) {
		$this->users = array();

		$this->loadAdmin();
		$this->loadAllUsers();
	}

	/**
	 * Do we have this user in this list?
	 * @throws  Exception if user provided is not in list
	 * @param  UserCredentials $fromClient
	 * @return UserCredentials from list
	 */
	public function findUser(UserCredentials $fromClient) {
		foreach($this->users as $user) {
			if ($user->isSame($fromClient) ) {
				\Debug::log("found User");
				return  $user;
			}
		}
		throw new \Exception("No matching user");
	}

	/**
	 * If a user exists
	 * @param  UserCredentials $credentials
	 * @return bool
	 */
	public function userExists(UserCredentials $credentials)
	{
		foreach ($this->users as $user) {
			if ($user->getUserName() == $credentials->getUserName()) {
				return true;
			}
		}
		return false;
	}

	public function update(UserCredentials $changedUser) {
		//this user needs to be saved since temporary password changed
		$this->adminFile->writeItem($changedUser->getUserName(), $changedUser->toString());

		\Debug::log("wrote changed user to file", true, $changedUser);
		$this->users[$changedUser->getUserName()->__toString()] = $changedUser;
	}

	public function loadAllUsers() {
		// FIXME: TEMP
		$usersFile = new \common\model\PHPFileStorage("data/admin.php");

		// read all users into users-array
		foreach ($usersFile->readAll() as $userString) {
			$user = UserCredentials::fromString($userString);
			$this->users[$user->getUserName()->__toString()] = $user;
		}
	}

	/**
	 * Temporary function to store "Admin" user in file "data/admin.php"
	 * If no file is found a new one is created.
	 *
	 * @return [type] [description]
	 */
	private function loadAdmin() {

		$this->adminFile = new \common\model\PHPFileStorage("data/admin.php");
		try {
			//Read admin from file
			$adminUserString = $this->adminFile->readItem("Admin");
			$admin = UserCredentials::fromString($adminUserString);

		} catch (\Exception $e) {
			\Debug::log("Could not read file, creating new one", true, $e);

			//Create a new user
			$userName = new UserName("Admin");
			$password = Password::fromCleartext("Password");
			$admin = UserCredentials::create( $userName, $password);
			$this->update($admin);
		}

		// Done in loadAllUsers
		//$this->users[$admin->getUserName()->__toString()] = $admin;
	}
}
